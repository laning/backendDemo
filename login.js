﻿var http = require('http');
var url = require('url');
var qs = require('querystring');

http.createServer(function (request,response) {
    response.writeHead(200,{'Content-Type':'text/html; charset=utf-8','Access-Control-Allow-Origin':'*'});
    //response.writeHead(200,{'Content-Type':'text/html; charset=utf-8'});
    if(url.parse(request.url,true).pathname === '/favicon.ico'){
        return;
    }
    var req_url = url.parse(request.url,true).query;
    request.setEncoding('utf8');
    var data = '';
    request.on('data',function (result) {
        data += result;
    });
    request.on('end',function () {
        console.log("data:" + data);
        var obj = {};
        var arr = data.split("&");
        if(arr.length === 2) {
            var username = arr[0].split("=")[1];
            var password = arr[1].split("=")[1];

            if (username === "admin" && password === "123" || decodeURI(username) === "管理员" && password === "123") {
                obj.code = 20;
                obj.msg = "登陆成功";
                obj.username = username;
            } else {
                obj.code = 40;
                obj.msg = "用户名或密码有误";
            }

        }else{
            obj.code = 42;
            obj.msg = "参数错误";
        }

        response.write(JSON.stringify(obj));
        response.end();
    })

}).listen(3000,function (err) {
    if (!err){
        console.log("服务器开启成功，端口为3000!");
    }
});
