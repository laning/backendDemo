﻿var http = require('http');
var url = require('url');
var qs = require('querystring');
var fs = require('fs');
var crypto = require('crypto');

http.createServer(function (request,response) {
    // response.writeHead(200,{'Content-Type':'text/html; charset=utf-8','Access-Control-Allow-Origin':'*'});
    // response.writeHead(200,{'Content-Type':'text/html; charset=utf-8'});
    response.setHeader('Content-Type', 'text/html; charset=utf-8');
    response.setHeader('Access-Control-Allow-Origin', "*");

    if(url.parse(request.url,true).pathname === '/favicon.ico'){
        return;
    }
    var req_url = url.parse(request.url,true).query;
    request.setEncoding('utf8');
    var data = '';
    request.on('data',function (result) {
        data += result;
    });
    request.on('end',function () {
        console.log("data:" + data);
        var obj = {};
        var arr = data.split("&");
        if(arr.length === 3) {
            var username = arr[0].split("=")[1];
            var password = arr[1].split("=")[1];
            var email = arr[2].split("=")[1];

            // 读取文件
            var users = [];
            fs.readFile('d:/users.txt', function (err, data) {
                if (err) {
                    console.log(err);
                    return;
                } else{
                    if(data.toString().length !== 0){
                        users = JSON.parse(data.toString());
                    }
                    users.push({
                         username: username,
                         password: cryptPwd(password),
                         email: email
                     });
                }
                // 写入文件
                fs.writeFile("d:/users.txt", JSON.stringify(users), function(err){
                    if(err){
                        obj.code = 50;
                        obj.msg = "注册失败";
                    }else{
                        obj.code = 20;
                        obj.msg = "注册成功";
                        obj.username = username;
                    }

                    response.write(JSON.stringify(obj));
                    response.end();
                });
            });

        }else{
            obj.code = 42;
            obj.msg = "参数错误";
            response.write(JSON.stringify(obj));
            response.end();
        }


    })

}).listen(3000,function (err) {
    if (!err){
        console.log("服务器开启成功，端口为3000!");
    }
});

function cryptPwd(password) {
    var md5 = crypto.createHash('md5');
    return md5.update(password).digest('hex');
}


