﻿var http = require('http');
var url = require('url');
var qs = require('querystring');

http.createServer(function (request, response) {
    response.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8', 'Access-Control-Allow-Origin': '*' });
    //response.writeHead(200,{'Content-Type':'text/html; charset=utf-8'});
    if (url.parse(request.url, true).pathname === '/favicon.ico') {
        return;
    }
    var req_url = url.parse(request.url, true).query;
    request.setEncoding('utf8');
    var data = '';
    request.on('data', function (result) {
        data += result;
    });
    request.on('end', function () {
        console.log("data:" + data);
        var obj = {};
        try {
            obj = JSON.parse(data);
            if (obj != null) {
                var username = obj.username;
                var password = obj.password;

                if (username === "admin" && password === "123" || decodeURI(username) === "管理员" && password === "123") {
                    obj.code = 20;
                    obj.msg = "登陆成功";
                    obj.username = username;
                } else {
                    obj.code = 40;
                    obj.msg = "用户名或密码有误";
                }

            } else {
                obj.code = 42;
                obj.msg = "参数错误";
            }
        } catch (error) {
            obj.code = 49;
            obj.msg = "参数格式错误,转换异常";
        } finally {
            response.write(JSON.stringify(obj));
            response.end();
        }
    })

}).listen(7000, function (err) {
    if (!err) {
        console.log("服务器开启成功，端口为7000!");
    }
});
